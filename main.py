import asyncio
from pyrogram import Client
from pyrogram import enums
from dotenv import dotenv_values

config = dotenv_values(".env")


async def main():
    async with Client("pgvdw_session", config['API_ID'], config['API_HASH']) as app:
        duration_all_msgs = 0
        voice_messages = []
        async for i in app.search_messages(chat_id=int(config['CHAT_ID']), filter=enums.MessagesFilter.VOICE_NOTE,
                                           limit=int(config['PARSING_LIMIT'])):
            if i.from_user.id == int(config['TARGET_ID']):
                duration_all_msgs += i.voice.duration
                print(f"{i.from_user.first_name} | {i.voice.duration} | {i.voice.file_id} | {i.voice.file_unique_id	}")
                voice_messages.append({
                    "from_user": i.from_user.id,
                    "in_chat": i.chat.id,
                    "file_id": i.voice.file_id,
                    "file_unique_id": i.voice.file_unique_id,
                    "duration": i.voice.duration,
                    "date": i.voice.date.isoformat()
                })
        print(f"duration of all msgs: {duration_all_msgs} in seconds; {duration_all_msgs/60} in minutes;"
              f" {duration_all_msgs/60/60} in hours")
        print(f"downloading {len(voice_messages)} voice msgs...")
        downloaded = 0
        for i in voice_messages:
            print(f"downloading {i['from_user']} | {i['duration']} | {i['date']}")
            await app.download_media(i['file_id'],
                                     file_name=config['SAVE_DIRECTORY']+"/"+f"{i['from_user']}-{i['duration']}-{i['date']}.ogg")
            print("file written to "+config['SAVE_DIRECTORY']+"/"+f"{i['from_user']}-{i['duration']}-{i['date']}.ogg")
            downloaded += 1
            print(f"downloaded {downloaded}/{len(voice_messages)}\n")
        print("downloaded!")

if __name__ == "__main__":
    print("download voice msgs from telegram - userm0de.com 2023")
    asyncio.run(main())
